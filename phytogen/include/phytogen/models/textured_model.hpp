#pragma once

#include <phytogen/textures/model_texture.hpp>

#include "raw_model.hpp"

namespace models
{
struct textured_model
{
private:
  raw_model m_raw_model;
  textures::model_texture m_texture;

public:
  textured_model(raw_model model, textures::model_texture texture);

  [[nodiscard]] raw_model raw() const;
  [[nodiscard]] textures::model_texture texture() const;
};
}  // namespace models
