#pragma once

#include <cstddef>

#include <glad/glad.h>

namespace models
{
struct raw_model
{
private:
  GLuint m_vao_id;
  GLsizei m_vertex_count;

public:
  raw_model(GLuint vao_id, GLsizei vertex_count);

  [[nodiscard]] GLuint vao_id() const;
  [[nodiscard]] GLsizei vertex_count() const;
};
} // namespace models
