#pragma once

#include <cstddef>

namespace textures
{
struct model_texture
{
private:
  size_t m_texture_id;

public:
  explicit model_texture(size_t id);

  [[nodiscard]] size_t id() const;
};
}  // namespace textures
