#pragma once

#include <string_view>

#include <config.hpp>
#include <glm/fwd.hpp>
#include <phytogen/entities/camera.hpp>

#include "shader_program.hpp"

namespace shaders
{
struct static_shader : shader_program
{
private:
  static constexpr std::string_view vertex_file =
      CMAKE_ROOT_DIR "/shaders/vertex_shader.txt";
  static constexpr std::string_view fragment_file =
      CMAKE_ROOT_DIR "/shaders/fragment_shader.txt";

  unsigned int m_location_transformation_matrix;
  unsigned int m_location_projection_matrix;
  unsigned int m_location_view_matrix;

public:
  static_shader();
  void load_transformation_matrix(const glm::mat4& matrix) const;
  void load_projection_matrix(const glm::mat4& matrix) const;
  void load_view_matrix(const entities::camera& camera);

protected:
  void bind_attributes() override;
  void get_all_uniform_locations() override;
};
}  // namespace shaders
