#pragma once

#include <string>
#include <string_view>

#include <glad/glad.h>
#include <glm/glm.hpp>

namespace shaders
{
struct shader_program
{
private:
  GLuint m_program_id;
  GLuint m_vertex_shader_id;
  GLuint m_fragment_shader_id;

public:
  shader_program(std::string_view vertex_file, std::string_view fragment_file);
  ~shader_program();
  shader_program(const shader_program&) = delete;
  shader_program& operator=(const shader_program&) = delete;
  shader_program(shader_program&&) = default;
  shader_program& operator=(shader_program&&) = default;

  void start() const;
  static void stop();

protected:
  virtual void bind_attributes() = 0;
  virtual void get_all_uniform_locations() = 0;
  void bind_attribute(GLuint attribute, const std::string& variable_name) const;
  void finish_init() const;
  [[nodiscard]] unsigned int get_uniform_location(
      std::string_view uniform_name) const;
  static void load_float(unsigned int location, float value);
  static void load_vector(unsigned int location, glm::vec3 vector);
  static void load_boolean(unsigned int location, bool value);
  static void load_matrix(unsigned int location, const glm::mat4& matrix);

private:
  static GLuint load_shader(std::string_view file, int type);
};
}  // namespace shaders
