#pragma once

#include <glm/glm.hpp>
#include <phytogen/models/textured_model.hpp>

namespace entities
{
struct entity
{
private:
  models::textured_model m_model;
  glm::vec3 m_position;
  float m_rot_x;
  float m_rot_y;
  float m_rot_z;
  float m_scale;

public:
  entity(models::textured_model model,
         glm::vec3 position,
         float rot_x,
         float rot_y,
         float rot_z,
         float scale);

  void increase_position(float dx, float dy, float dz);
  void increase_rotation(float dx, float dy, float dz);

  [[nodiscard]] models::textured_model model() const;
  void set_model(const models::textured_model& model);
  [[nodiscard]] glm::vec3 position() const;
  void set_position(const glm::vec3& position);
  [[nodiscard]] float rot_x() const;
  void set_rot_x(float rot_x);
  [[nodiscard]] float rot_y() const;
  void set_rot_y(float rot_y);
  [[nodiscard]] float rot_z() const;
  void set_rot_z(float rot_z);
  [[nodiscard]] float scale() const;
  void set_scale(float scale);
};
}  // namespace entities
