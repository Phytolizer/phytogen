#pragma once

#include <glm/glm.hpp>
#include <phytogen/render_engine/display_manager.hpp>

namespace entities
{
struct camera
{
private:
  glm::vec3 m_position = {0, 0, 0};
  float m_pitch = 0;
  float m_yaw = 0;
  float m_roll = 0;

public:
  camera() = default;

  void move(const render_engine::display_manager& display_manager);

  [[nodiscard]] glm::vec3 position() const;
  [[nodiscard]] float pitch() const;
  [[nodiscard]] float yaw() const;
  [[nodiscard]] float roll() const;
};
}  // namespace entities
