#pragma once

extern "C" {
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <glad/glad.h>
}

namespace render_engine
{
struct display_manager
{
  GLFWwindow* handle = nullptr;

  display_manager();
  ~display_manager();
  display_manager(const display_manager&) = delete;
  display_manager& operator=(const display_manager&) = delete;
  display_manager(display_manager&&) = default;
  display_manager& operator=(display_manager&&) = default;

  void update_display() const;
  [[nodiscard]] int get_display_width() const;
  [[nodiscard]] int get_display_height() const;
  [[nodiscard]] bool get_key(int key) const;

  [[nodiscard]] bool should_close() const;
};
}  // namespace render_engine
