#pragma once

#include <string_view>
#include <vector>

#include <glad/glad.h>
#include <phytogen/models/raw_model.hpp>

#include "raw_texture.hpp"

namespace render_engine
{
struct loader
{
private:
  std::vector<GLuint> m_vaos;
  std::vector<GLuint> m_vbos;
  std::vector<GLuint> m_textures;

public:
  ~loader();
  loader() = default;
  loader(const loader&) = delete;
  loader& operator=(const loader&) = delete;
  loader(loader&&) = default;
  loader& operator=(loader&&) = default;

  models::raw_model load_to_vao(const std::vector<float>& positions,
                                const std::vector<float>& texture_coords,
                                const std::vector<unsigned int>& indices);
  GLuint load_texture(std::string_view file_name);

private:
  GLuint create_vao();
  void store_data_in_attribute_list(GLuint attribute_number,
                                    GLint coordinate_size,
                                    const std::vector<float>& data);
  static void unbind_vao();
  void bind_indices_buffer(const std::vector<unsigned int>& indices);
  static void store_data_in_int_buffer(const std::vector<unsigned int>& data);
  static void store_data_in_float_buffer(const std::vector<float>& data);
};
}  // namespace render_engine
