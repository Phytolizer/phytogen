#pragma once

namespace render_engine
{
struct raw_texture
{
  unsigned char* data;
  int width;
  int height;
  int channel_count;

  raw_texture(unsigned char* data, int width, int height, int channel_count);
  ~raw_texture();
  raw_texture(const raw_texture&) = delete;
  raw_texture& operator=(const raw_texture&) = delete;
  raw_texture(raw_texture&&) = default;
  raw_texture& operator=(raw_texture&&) = default;
};
}  // namespace render_engine
