#pragma once

#include <string_view>

#include <phytogen/models/raw_model.hpp>

#include "loader.hpp"
namespace render_engine
{
struct obj_loader
{
  static models::raw_model load_obj_model(std::string_view file_name,
                                          loader& loader);
};
}  // namespace render_engine
