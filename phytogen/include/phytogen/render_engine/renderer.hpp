#pragma once

#include <glm/glm.hpp>
#include <phytogen/entities/entity.hpp>
#include <phytogen/render_engine/display_manager.hpp>
#include <phytogen/shaders/static_shader.hpp>

namespace render_engine
{
struct renderer
{
private:
  static constexpr float fov = 70;
  static constexpr float near_plane = 0.1;
  static constexpr float far_plane = 1000;
  glm::mat4 m_projection_matrix;

public:
  explicit renderer(const display_manager& display_manager,
                    const shaders::static_shader& shader);
  void prepare() const;
  void render(const entities::entity& entity,
              const shaders::static_shader& shader) const;

private:
  void create_projection_matrix(const display_manager& display_manager);
};
}  // namespace render_engine
