#pragma once

#include <glm/fwd.hpp>
#include <phytogen/entities/camera.hpp>

namespace toolbox::math
{
glm::mat4 create_transformation_matrix(
    glm::vec3 translation, float rx, float ry, float rz, float scale);
glm::mat4 create_view_matrix(const entities::camera& camera);
}  // namespace toolbox::math
