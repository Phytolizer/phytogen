if(PROJECT_IS_TOP_LEVEL)
  set(CMAKE_INSTALL_INCLUDEDIR include/phytogen CACHE PATH "")
endif()

include(CMakePackageConfigHelpers)
include(GNUInstallDirs)

# find_package(<package>) call for consumers to find this project
set(package phytogen)

install(
    TARGETS phytogen_phytogen
    EXPORT phytogenTargets
    RUNTIME COMPONENT phytogen_Runtime
)

write_basic_package_version_file(
    "${package}ConfigVersion.cmake"
    COMPATIBILITY SameMajorVersion
)

# Allow package maintainers to freely override the path for the configs
set(
    phytogen_INSTALL_CMAKEDIR "${CMAKE_INSTALL_DATADIR}/${package}"
    CACHE PATH "CMake package config location relative to the install prefix"
)
mark_as_advanced(phytogen_INSTALL_CMAKEDIR)

install(
    FILES cmake/install-config.cmake
    DESTINATION "${phytogen_INSTALL_CMAKEDIR}"
    RENAME "${package}Config.cmake"
    COMPONENT phytogen_Development
)

install(
    FILES "${PROJECT_BINARY_DIR}/${package}ConfigVersion.cmake"
    DESTINATION "${phytogen_INSTALL_CMAKEDIR}"
    COMPONENT phytogen_Development
)

install(
    EXPORT phytogenTargets
    NAMESPACE phytogen::
    DESTINATION "${phytogen_INSTALL_CMAKEDIR}"
    COMPONENT phytogen_Development
)

if(PROJECT_IS_TOP_LEVEL)
  include(CPack)
endif()
