#include <iostream>
#include <stdexcept>
#include <string>

#include <config.hpp>
#include <phytogen/models/textured_model.hpp>
#include <phytogen/render_engine/display_manager.hpp>
#include <phytogen/render_engine/loader.hpp>
#include <phytogen/render_engine/obj_loader.hpp>
#include <phytogen/render_engine/renderer.hpp>
#include <phytogen/shaders/static_shader.hpp>
#include <phytogen/textures/model_texture.hpp>

int main()
{
  // Initialize the display manager.
  render_engine::display_manager display_manager;

  render_engine::loader loader;
  shaders::static_shader shader;
  auto renderer = render_engine::renderer {display_manager, shader};

  auto model = render_engine::obj_loader::load_obj_model(
      CMAKE_ROOT_DIR "/resources/stall.obj", loader);
  textures::model_texture texture {
      loader.load_texture(CMAKE_ROOT_DIR "/resources/stallTexture.png")};
  auto textured_model = models::textured_model {model, texture};

  auto entity =
      entities::entity {textured_model, glm::vec3 {0, 0, -50}, 0, 0, 0, 1};

  auto camera = entities::camera {};

  // Render stuff!
  while (!display_manager.should_close()) {
    entity.increase_rotation(0, 1, 0);
    renderer.prepare();
    shader.start();
    camera.move(display_manager);
    shader.load_view_matrix(camera);
    renderer.render(entity, shader);
    shaders::static_shader::stop();
    display_manager.update_display();
  }

  // Done here.
  return 0;
}
