#include "phytogen/textures/model_texture.hpp"

textures::model_texture::model_texture(size_t id)
    : m_texture_id(id)
{
}

size_t textures::model_texture::id() const {
  return m_texture_id;
}
