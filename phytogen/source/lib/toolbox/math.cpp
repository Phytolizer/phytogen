#include "phytogen/toolbox/math.hpp"

#include <glm/ext/matrix_transform.hpp>
#include <glm/glm.hpp>

glm::mat4 toolbox::math::create_transformation_matrix(
    glm::vec3 translation, float rx, float ry, float rz, float scale)
{
  auto matrix = glm::identity<glm::mat4>();
  matrix = glm::translate(matrix, translation);
  matrix = glm::rotate(matrix, glm::radians(rx), glm::vec3 {1, 0, 0});
  matrix = glm::rotate(matrix, glm::radians(ry), glm::vec3 {0, 1, 0});
  matrix = glm::rotate(matrix, glm::radians(rz), glm::vec3 {0, 0, 1});
  matrix = glm::scale(matrix, glm::vec3 {scale, scale, scale});
  return matrix;
}

glm::mat4 toolbox::math::create_view_matrix(const entities::camera& camera)
{
  auto view_matrix = glm::identity<glm::mat4>();
  view_matrix = glm::rotate(
      view_matrix, glm::radians(camera.pitch()), glm::vec3 {1, 0, 0});
  view_matrix =
      glm::rotate(view_matrix, glm::radians(camera.yaw()), glm::vec3 {0, 1, 0});
  view_matrix = glm::rotate(
      view_matrix, glm::radians(camera.roll()), glm::vec3 {0, 0, 1});
  view_matrix = glm::translate(view_matrix, -camera.position());
  return view_matrix;
}
