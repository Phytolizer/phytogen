#include "phytogen/entities/camera.hpp"

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

void entities::camera::move(
    const render_engine::display_manager& display_manager)
{
  if (display_manager.get_key(GLFW_KEY_W)) {
    m_position.z -= 0.02;
  }
  if (display_manager.get_key(GLFW_KEY_S)) {
    m_position.z += 0.02;
  }
  if (display_manager.get_key(GLFW_KEY_D)) {
    m_position.x += 0.02;
  }
  if (display_manager.get_key(GLFW_KEY_A)) {
    m_position.x -= 0.02;
  }
}

glm::vec3 entities::camera::position() const
{
  return m_position;
}

float entities::camera::pitch() const
{
  return m_pitch;
}

float entities::camera::yaw() const
{
  return m_yaw;
}

float entities::camera::roll() const
{
  return m_roll;
}
