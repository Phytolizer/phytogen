#include "phytogen/entities/entity.hpp"

entities::entity::entity(models::textured_model model,
                         glm::vec3 position,
                         float rot_x,
                         float rot_y,
                         float rot_z,
                         float scale)
    : m_model(model)
    , m_position(position)
    , m_rot_x(rot_x)
    , m_rot_y(rot_y)
    , m_rot_z(rot_z)
    , m_scale(scale)
{
}

void entities::entity::increase_position(float dx, float dy, float dz)
{
  m_position.x += dx;
  m_position.y += dy;
  m_position.z += dz;
}

void entities::entity::increase_rotation(float dx, float dy, float dz)
{
  m_rot_x += dx;
  m_rot_y += dy;
  m_rot_z += dz;
}

models::textured_model entities::entity::model() const
{
  return m_model;
}

void entities::entity::set_model(const models::textured_model& model)
{
  m_model = model;
}

glm::vec3 entities::entity::position() const
{
  return m_position;
}

void entities::entity::set_position(const glm::vec3& position)
{
  m_position = position;
}

float entities::entity::rot_x() const
{
  return m_rot_x;
}

void entities::entity::set_rot_x(float rot_x)
{
  m_rot_x = rot_x;
}

float entities::entity::rot_y() const
{
  return m_rot_y;
}

void entities::entity::set_rot_y(float rot_y)
{
  m_rot_y = rot_y;
}

float entities::entity::rot_z() const
{
  return m_rot_z;
}

void entities::entity::set_rot_z(float rot_z)
{
  m_rot_z = rot_z;
}

float entities::entity::scale() const
{
  return m_scale;
}

void entities::entity::set_scale(float scale)
{
  m_scale = scale;
}
