#include <utility>

#include "phytogen/models/textured_model.hpp"

models::textured_model::textured_model(models::raw_model model,
                                       textures::model_texture texture)
    : m_raw_model(model)
    , m_texture(texture)
{
}

models::raw_model models::textured_model::raw() const
{
  return m_raw_model;
}

textures::model_texture models::textured_model::texture() const
{
  return m_texture;
}
