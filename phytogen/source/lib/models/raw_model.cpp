#include "phytogen/models/raw_model.hpp"

models::raw_model::raw_model(GLuint vao_id, GLsizei vertex_count)
    : m_vao_id(vao_id)
    , m_vertex_count(vertex_count)
{
}

GLuint models::raw_model::vao_id() const
{
  return m_vao_id;
}

GLsizei models::raw_model::vertex_count() const
{
  return m_vertex_count;
}
