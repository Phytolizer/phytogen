#include <iostream>
#include <stdexcept>

#include "phytogen/render_engine/display_manager.hpp"

#include <GLFW/glfw3.h>

constexpr int width = 1280;
constexpr int height = 720;

static void glfw_error_callback(int error, const char* description)
{
  std::cerr << "[GLFW] Error " << error << ": " << description << std::endl;
}

render_engine::display_manager::display_manager()
{
  if (glfwInit() == 0) {
    throw std::runtime_error {"GLFW initialization failed. Cannot continue."};
  }

  glfwSetErrorCallback(glfw_error_callback);

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
  handle = glfwCreateWindow(width, height, "GLFW window", nullptr, nullptr);
  if (handle == nullptr) {
    throw std::runtime_error {"Window creation failed. Cannot continue."};
  }

  glfwMakeContextCurrent(handle);
  // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
  gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress));

  glViewport(0, 0, width, height);
  glfwSwapInterval(1);
}

void render_engine::display_manager::update_display() const
{
  glfwSwapBuffers(handle);
  glfwPollEvents();
}

int render_engine::display_manager::get_display_width() const
{
  int width = 0;
  glfwGetWindowSize(handle, &width, nullptr);
  return width;
}

int render_engine::display_manager::get_display_height() const
{
  int height = 0;
  glfwGetWindowSize(handle, nullptr, &height);
  return height;
}

bool render_engine::display_manager::get_key(int key) const
{
  return glfwGetKey(handle, key) == GLFW_PRESS;
}

bool render_engine::display_manager::should_close() const
{
  return glfwWindowShouldClose(handle) != 0;
}

render_engine::display_manager::~display_manager()
{
  if (handle != nullptr) {
    glfwDestroyWindow(handle);
  }

  glfwTerminate();
}
