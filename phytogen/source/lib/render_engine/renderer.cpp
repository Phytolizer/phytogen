#include "phytogen/render_engine/renderer.hpp"

#include <glad/glad.h>
#include <glm/ext/matrix_transform.hpp>
#include <phytogen/toolbox/math.hpp>

render_engine::renderer::renderer(const display_manager& display_manager,
                                  const shaders::static_shader& shader)
    : m_projection_matrix(glm::identity<glm::mat4>())
{
  create_projection_matrix(display_manager);
  shader.start();
  shader.load_projection_matrix(m_projection_matrix);
  shaders::static_shader::stop();
}

void render_engine::renderer::prepare() const
{
  glEnable(GL_DEPTH_TEST);
  glClearColor(1, 0, 0, 1);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void render_engine::renderer::render(const entities::entity& entity,
                                     const shaders::static_shader& shader) const
{
  auto textured_model = entity.model();
  auto model = textured_model.raw();
  glBindVertexArray(model.vao_id());
  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);
  auto transformation_matrix =
      toolbox::math::create_transformation_matrix(entity.position(),
                                                  entity.rot_x(),
                                                  entity.rot_y(),
                                                  entity.rot_z(),
                                                  entity.scale());
  shader.load_transformation_matrix(transformation_matrix);
  glBindTexture(GL_TEXTURE_2D, textured_model.texture().id());
  glDrawElements(GL_TRIANGLES, model.vertex_count(), GL_UNSIGNED_INT, nullptr);
  glDisableVertexAttribArray(1);
  glDisableVertexAttribArray(0);
  glBindVertexArray(0);
}

void render_engine::renderer::create_projection_matrix(
    const display_manager& display_manager)
{
  float aspect_ratio = static_cast<float>(display_manager.get_display_width())
      / static_cast<float>(display_manager.get_display_height());
  float y_scale =
      static_cast<float>(1.0 / tan(glm::radians(fov / 2.0))) * aspect_ratio;
  float x_scale = y_scale / aspect_ratio;
  float frustum_length = far_plane - near_plane;

  m_projection_matrix[0][0] = x_scale;
  m_projection_matrix[1][1] = y_scale;
  m_projection_matrix[2][2] = -((far_plane + near_plane) / frustum_length);
  m_projection_matrix[2][3] = -1;
  m_projection_matrix[3][2] = -((2 * near_plane * far_plane) / frustum_length);
  m_projection_matrix[3][3] = 0;
}
