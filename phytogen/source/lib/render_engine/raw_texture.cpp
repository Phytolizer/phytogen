#include <cstdlib>

#include "phytogen/render_engine/raw_texture.hpp"

render_engine::raw_texture::raw_texture(unsigned char* data,
                                        int width,
                                        int height,
                                        int channel_count)
    : data(data)
    , width(width)
    , height(height)
    , channel_count(channel_count)
{
}

render_engine::raw_texture::~raw_texture()
{
  // explanation: the following memory was allocated by stb_image and needs to
  // be freed manually with free(). there is nothing to be done about this.
  // NOLINTNEXTLINE(cppcoreguidelines-owning-memory,hicpp-no-malloc,cppcoreguidelines-no-malloc)
  free(data);
}
