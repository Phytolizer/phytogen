#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "phytogen/render_engine/obj_loader.hpp"

#include <glm/glm.hpp>
#include <tiny_obj_loader.h>

models::raw_model render_engine::obj_loader::load_obj_model(
    std::string_view file_name, loader& loader)
{
  tinyobj::ObjReaderConfig config;
  tinyobj::ObjReader reader;
  if (!reader.ParseFromFile(std::string {file_name}, config)) {
    if (!reader.Error().empty()) {
      std::cerr << "[tiny_obj_reader] error: " << reader.Error() << "\n";
    }

    throw std::runtime_error {"failed to parse " + std::string {file_name}};
  }

  if (!reader.Warning().empty()) {
    std::cerr << "[tiny_obj_reader] warning: " << reader.Warning() << "\n";
  }

  const auto& attrib = reader.GetAttrib();
  const auto& shapes = reader.GetShapes();
  std::vector<float> vertices = attrib.vertices;
  std::vector<unsigned int> indices;
  std::vector<float> texcoords;
  texcoords.resize(attrib.texcoords.size());
  for (const auto& shape : shapes) {
    size_t index_offset = 0;
    for (const auto& fv : shape.mesh.num_face_vertices) {
      for (size_t v = 0; v < fv; ++v) {
        tinyobj::index_t idx = shape.mesh.indices[index_offset + v];
        // tinyobj::real_t vx =
        //     attrib.vertices[3 * static_cast<size_t>(idx.vertex_index) + 0];
        // tinyobj::real_t vy =
        //     attrib.vertices[3 * static_cast<size_t>(idx.vertex_index) + 1];
        // tinyobj::real_t vz =
        //     attrib.vertices[3 * static_cast<size_t>(idx.vertex_index) + 2];
        indices.push_back(static_cast<unsigned int>(idx.vertex_index));

        if (idx.normal_index >= 0) {
          // tinyobj::real_t nx =
          //     attrib.normals[3 * static_cast<size_t>(idx.normal_index) + 0];
          // tinyobj::real_t ny =
          //     attrib.normals[3 * static_cast<size_t>(idx.normal_index) + 1];
          // tinyobj::real_t nz =
          //     attrib.normals[3 * static_cast<size_t>(idx.normal_index) + 2];
          // normals.emplace_back(nx, ny, nz);
        }

        if (idx.texcoord_index >= 0) {
          tinyobj::real_t tx =
              attrib.texcoords[3 * static_cast<size_t>(idx.texcoord_index) + 0];
          tinyobj::real_t ty =
              attrib.texcoords[3 * static_cast<size_t>(idx.texcoord_index) + 1];
          texcoords[2 * static_cast<size_t>(idx.vertex_index) + 0] = tx;
          texcoords[2 * static_cast<size_t>(idx.vertex_index) + 1] = ty;
        }
      }

      index_offset += fv;
    }
  }

  return loader.load_to_vao(vertices, texcoords, indices);
}
