#include <iostream>
#include <string>

#include "phytogen/render_engine/loader.hpp"

#include <stb_image.h>

render_engine::loader::~loader()
{
  glDeleteVertexArrays(static_cast<GLsizei>(m_vaos.size()), m_vaos.data());
  glDeleteBuffers(static_cast<GLsizei>(m_vbos.size()), m_vbos.data());
  glDeleteTextures(static_cast<GLsizei>(m_textures.size()), m_textures.data());
}

models::raw_model render_engine::loader::load_to_vao(
    const std::vector<float>& positions,
    const std::vector<float>& texture_coords,
    const std::vector<unsigned int>& indices)
{
  auto vao_id = create_vao();
  bind_indices_buffer(indices);
  store_data_in_attribute_list(0, 3, positions);
  store_data_in_attribute_list(1, 2, texture_coords);
  unbind_vao();
  return models::raw_model {vao_id, static_cast<GLsizei>(indices.size())};
}

GLuint render_engine::loader::load_texture(std::string_view file_name)
{
  int x = 0;
  int y = 0;
  int n = 0;
  unsigned char* texture =
      stbi_load(std::string {file_name}.c_str(), &x, &y, &n, 4);
  if (texture == nullptr) {
    throw std::runtime_error {stbi_failure_reason()};
  }
  GLuint texture_id = 0;
  glGenTextures(1, &texture_id);
  m_textures.push_back(texture_id);
  glBindTexture(GL_TEXTURE_2D, texture_id);
  glTexImage2D(
      GL_TEXTURE_2D, 0, GL_RGBA, x, y, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture);
  glGenerateMipmap(GL_TEXTURE_2D);
  stbi_image_free(texture);
  return texture_id;
}

GLuint render_engine::loader::create_vao()
{
  GLuint vao_id = 0;
  glGenVertexArrays(1, &vao_id);
  m_vaos.push_back(vao_id);
  glBindVertexArray(vao_id);
  return vao_id;
}

void render_engine::loader::store_data_in_attribute_list(
    GLuint attribute_number,
    GLint coordinate_size,
    const std::vector<float>& data)
{
  GLuint vbo_id = 0;
  glGenBuffers(1, &vbo_id);
  m_vbos.push_back(vbo_id);
  glBindBuffer(GL_ARRAY_BUFFER, vbo_id);
  store_data_in_float_buffer(data);
  glVertexAttribPointer(attribute_number,
                        coordinate_size,
                        GL_FLOAT,
                        0U,
                        0,
                        static_cast<const void*>(nullptr));
  glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void render_engine::loader::unbind_vao()
{
  glBindVertexArray(0);
}

void render_engine::loader::bind_indices_buffer(
    const std::vector<unsigned int>& indices)
{
  GLuint vbo_id = 0;
  glGenBuffers(1, &vbo_id);
  m_vbos.push_back(vbo_id);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_id);
  store_data_in_int_buffer(indices);
}

void render_engine::loader::store_data_in_int_buffer(
    const std::vector<unsigned int>& data)
{
  glBufferData(GL_ELEMENT_ARRAY_BUFFER,
               static_cast<GLsizeiptr>(data.size() * sizeof(int)),
               data.data(),
               GL_STATIC_DRAW);
}

void render_engine::loader::store_data_in_float_buffer(
    const std::vector<float>& data)
{
  glBufferData(GL_ARRAY_BUFFER,
               static_cast<GLsizeiptr>(data.size() * sizeof(float)),
               data.data(),
               GL_STATIC_DRAW);
}
