#include "phytogen/shaders/static_shader.hpp"

#include <phytogen/toolbox/math.hpp>

shaders::static_shader::static_shader()
    : shader_program(vertex_file, fragment_file)
    // dummy value to silence warning, this is computed in
    // get_all_uniform_locations()
    , m_location_transformation_matrix(0)
    , m_location_projection_matrix(0)
    , m_location_view_matrix(0)
{
  bind_attributes();
  finish_init();
  get_all_uniform_locations();
}

void shaders::static_shader::load_transformation_matrix(
    const glm::mat4& matrix) const
{
  load_matrix(m_location_transformation_matrix, matrix);
}

void shaders::static_shader::load_projection_matrix(
    const glm::mat4& matrix) const
{
  load_matrix(m_location_projection_matrix, matrix);
}

void shaders::static_shader::load_view_matrix(const entities::camera& camera)
{
  glm::mat4 view_matrix = toolbox::math::create_view_matrix(camera);
  load_matrix(m_location_view_matrix, view_matrix);
}

void shaders::static_shader::bind_attributes()
{
  bind_attribute(0, "position");
  bind_attribute(1, "textureCoords");
}

void shaders::static_shader::get_all_uniform_locations()
{
  m_location_transformation_matrix =
      get_uniform_location("transformationMatrix");
  m_location_projection_matrix = get_uniform_location("projectionMatrix");
  m_location_view_matrix = get_uniform_location("viewMatrix");
}
