#include <array>
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>

#include "phytogen/shaders/shader_program.hpp"

#include <glad/glad.h>
#include <glm/gtc/type_ptr.hpp>

shaders::shader_program::shader_program(std::string_view vertex_file,
                                        std::string_view fragment_file)
{
  m_vertex_shader_id = load_shader(vertex_file, GL_VERTEX_SHADER);
  m_fragment_shader_id = load_shader(fragment_file, GL_FRAGMENT_SHADER);
  m_program_id = glCreateProgram();
  glAttachShader(m_program_id, m_vertex_shader_id);
  glAttachShader(m_program_id, m_fragment_shader_id);
}

shaders::shader_program::~shader_program()
{
  stop();
  glDetachShader(m_program_id, m_vertex_shader_id);
  glDetachShader(m_program_id, m_fragment_shader_id);
  glDeleteShader(m_vertex_shader_id);
  glDeleteShader(m_fragment_shader_id);
  glDeleteProgram(m_program_id);
}

void shaders::shader_program::start() const
{
  glUseProgram(m_program_id);
}

void shaders::shader_program::stop()
{
  glUseProgram(0);
}

void shaders::shader_program::bind_attribute(
    GLuint attribute, const std::string& variable_name) const
{
  glBindAttribLocation(m_program_id, attribute, variable_name.c_str());
}

void shaders::shader_program::finish_init() const
{
  glLinkProgram(m_program_id);
  glValidateProgram(m_program_id);
}

unsigned int shaders::shader_program::get_uniform_location(
    std::string_view uniform_name) const
{
  return glGetUniformLocation(m_program_id, std::string {uniform_name}.c_str());
}

void shaders::shader_program::load_float(unsigned int location, float value)
{
  glUniform1f(static_cast<GLint>(location), value);
}

void shaders::shader_program::load_vector(unsigned int location,
                                          glm::vec3 vector)
{
  glUniform3f(static_cast<GLint>(location), vector.x, vector.y, vector.z);
}

void shaders::shader_program::load_boolean(unsigned int location, bool value)
{
  glUniform1f(static_cast<GLint>(location), value ? 1 : 0);
}

void shaders::shader_program::load_matrix(unsigned int location,
                                          const glm::mat4& matrix)
{
  glUniformMatrix4fv(
      static_cast<GLint>(location), 1, GL_FALSE, glm::value_ptr(matrix));
}

GLuint shaders::shader_program::load_shader(std::string_view file, int type)
{
  std::ifstream reader {std::string {file.begin(), file.end()}};
  if (!reader) {
    throw std::runtime_error {"Could not read shader file!"};
  }

  std::string line;
  std::stringstream shader_source_stream;
  while (std::getline(reader, line)) {
    shader_source_stream << line << "\n";
  }
  std::string shader_source = shader_source_stream.str();
  GLuint shader_id = glCreateShader(type);
  const GLchar* shader_source_raw = shader_source.c_str();
  auto shader_source_length = static_cast<GLint>(shader_source.size());
  glShaderSource(shader_id, 1, &shader_source_raw, &shader_source_length);
  glCompileShader(shader_id);
  GLint status = 0;
  glGetShaderiv(shader_id, GL_COMPILE_STATUS, &status);
  if (status != GL_TRUE) {
    std::array<GLchar, 500> raw_info_log = {0};
    GLsizei log_length = 0;
    glGetShaderInfoLog(shader_id, 500, &log_length, raw_info_log.data());
    std::string info_log = raw_info_log.data();
    std::cerr << info_log << "\n";
    throw std::runtime_error {"Could not compile shader!"};
  }
  return shader_id;
}
